import { Component, OnInit } from '@angular/core';
import { TodoDataService } from '../service/data/todo-data.service';
import {Router} from '@angular/router'
export class Todo{
  constructor(
    public id: number,
    public description: string,
    public done: boolean,
    public targetDate: Date
  ){

  }
}



@Component({
  selector: 'app-list-todos',
  templateUrl: './list-todos.component.html',
  styleUrls: ['./list-todos.component.css']
})
export class ListTodosComponent implements OnInit {
todos: Todo[]
message: string
//   todos = [
// new Todo(1, 'Learn to Code', false, new Date()),
// new Todo(2, 'Become angular expert', false, new Date()),
// new Todo(3, "Visit India", false, new Date())

//   //   {
//   //     id: 1,
//   //     description: 'Learn to code'
//   //   },
//   //   {
//   //     id: 2,
//   //     description: 'Expert in angular'
//   //   },
//   //   {
//   //     id: 3,
//   //     description: 'Visit Japan'
//   //   }
//   // ]
//   ]

  // todo = {
  //   id : 1,
  //   description: 'Learn to Code'
  // }

  constructor(
    private service:TodoDataService,
    private router: Router
  ) { }

  ngOnInit() {
    this.refreshTodos();
  }

  refreshTodos(){
    this.service.retrieveAllTodos('kirito').subscribe(
      response =>{
        console.log(response);
        this.todos = response;
      }
    )
  }

  deleteTodo(id){
    console.log(`Delete todo ${id}`);
    this.service.deleteTodo('kirito',id).subscribe(
      response => {
        console.log(response);
        this.message = `Delete of Todo ${id} successfull!`
        this.refreshTodos();
      }
    )
  }

  updateTodo(id){
    console.log(`Update todo ${id}`);
    this.router.navigate(['todos',id]);
  }

  addTodo() {
    this.router.navigate(['todos',-1]);
  }

}
