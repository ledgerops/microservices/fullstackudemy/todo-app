import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'

export class HelloWorldBean{
  constructor(public message: string){
    
  }
}


@Injectable({
  providedIn: 'root'
})
export class WelcomeDataService {

  constructor(
    private http: HttpClient
  ) { }

  executeHelloWorldBeanService() {

    console.log(this.http.get('http://localhost:8080/hello-world-bean'));

    return this.http.get('http://localhost:8080/hello-world-bean');
   // console.log("Execute Hello World Bean Service");
  }

  executeHelloWorldServiceWithPathVariable(name){

    // let basicAuthHeaderString = this.createBasicAuthenticationHttpHeader();

    // let header = new HttpHeaders({
    //   Authorization: basicAuthHeaderString
    // })
    //This is the best practice.
    return this.http.get(`http://localhost:8080/hello-world/path-variable/${name}`,
    // {
    //   headers: header
    // }
    );
  }

  // createBasicAuthenticationHttpHeader() {
  //   let username = 'user'
  //   let password = 'password'
  //   //byte64 encoded username and password
  //   let basicAuthHeader = 'Basic '+window.btoa(username+':'+password);
  //   return basicAuthHeader;
  // }
}
