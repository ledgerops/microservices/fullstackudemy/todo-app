import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { BasicAuthenticationService } from './BasicAuthenticationService';

@Injectable({
  providedIn: 'root'
})
export class HttpIntercepterBasicAuthService implements HttpInterceptor{

  constructor(private basicAuthService: BasicAuthenticationService) { }

  intercept(request: HttpRequest<any>, next: HttpHandler){

    // let username = 'user'
    // let password = 'password'
    // let basicAuthHeaderString = 'Basic '+window.btoa(username+':'+password);
    let basicAuthHeaderString = this.basicAuthService.getAuthenticatedToken();
    let username = this.basicAuthService.getAuthenticatedUser();

    if(basicAuthHeaderString && username){

    request = request.clone({
      setHeaders: {
        Authorization : basicAuthHeaderString
      }
    });
  }
    return next.handle(request);
  }
}
