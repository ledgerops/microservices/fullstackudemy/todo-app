import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {map} from 'rxjs/operators'
@Injectable({
  providedIn: 'root'
})
export class BasicAuthenticationService {

  constructor(private http: HttpClient) { }


  executeBasicAuthenticationService(username, password){
    // let username = 'user'
    // let password = 'password'
    //byte64 encoded username and password
    let basicAuthHeader = 'Basic '+window.btoa(username+':'+password);
    console.log('Basic header'+basicAuthHeader);
    //return basicAuthHeader;

    let basicAuthHeaderString = basicAuthHeader;

    //let basicAuthHeaderString = this.createBasicAuthenticationHttpHeader();

    let header = new HttpHeaders({
      Authorization: basicAuthHeaderString
    })
    //This is the best practice.
    return this.http.get<AuthenticationBean>(`http://localhost:8080/basicauth`,
    {
      headers: header
    }).pipe(
        map(
            data => {
                sessionStorage.setItem('authenticatedUser', username);
                sessionStorage.setItem('token', basicAuthHeader);
                return data;
            }
        )
    );
  }
  executeJWTAuthenticateService(username, password){
    // let username = 'user'
    // let password = 'password'
    //byte64 encoded username and password
    // let basicAuthHeader = 'Basic '+window.btoa(username+':'+password);
    // console.log('Basic header'+basicAuthHeader);
    // //return basicAuthHeader;

    // let basicAuthHeaderString = basicAuthHeader;

    // //let basicAuthHeaderString = this.createBasicAuthenticationHttpHeader();

    // let header = new HttpHeaders({
    //   Authorization: basicAuthHeaderString
    // })
    //This is the best practice.
    return this.http.post<any>(`http://localhost:8080/authenticate`, {
      username,
      password
    })
    .pipe(
        map(
            data => {
                sessionStorage.setItem('authenticatedUser', username);
                sessionStorage.setItem('token', `Bearer ${data.token}`);
                return data;
            }
        )
    );
  }













  createBasicAuthenticationHttpHeader() {

  }



//   authenticate(username, password){

//     console.log(this.isUserLoggedIn());

//     if(username === "kirito" && password === "dummy"){
//       sessionStorage.setItem('authenticatedUser', username)
//       console.log(this.isUserLoggedIn());
//       return true;
//     }
//     return false;
//   }

getAuthenticatedUser(){
    return sessionStorage.getItem('authenticatedUser');
}

getAuthenticatedToken() {
    return sessionStorage.getItem('token');
}


  isUserLoggedIn() {
    let user  = sessionStorage.getItem('authenticatedUser')
    return  !(user === null)
  }
  logout(){
    sessionStorage.removeItem('authenticatedUser')
    sessionStorage.removeItem('token')
  }
}

export class AuthenticationBean{
    constructor(public message:string){

    }
}