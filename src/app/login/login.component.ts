import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HardcodedAuthenticationService } from '../service/hardcoded-authentication.service';
import { BasicAuthenticationService } from '../service/http/BasicAuthenticationService';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username = "kirito"
  password = ""
  errorMessage = "Invalid credentials"
  invalidLogin = false

  //Router

  constructor(private router: Router, private hardcodedAuthenticationService: HardcodedAuthenticationService,
    private basicAuthService: BasicAuthenticationService) { 


  }

  ngOnInit() {
  }

  handleLogin() {

    // if(this.username === "kirito" && this.password === 'dummy'){
    if (this.hardcodedAuthenticationService.authenticate(this.username, this.password)) {
      //Redirect to welcome page
      this.router.navigate(['welcome', this.username])
      this.invalidLogin = false
    } else {
      this.invalidLogin = true
    }
    console.log(this.username);
  }


  handleBasicAuthLogin() {

    // if(this.username === "kirito" && this.password === 'dummy'){
    this.basicAuthService.executeBasicAuthenticationService(this.username, this.password)
    .subscribe(
      success =>{
        console.log(success)
              
      //Redirect to welcome page
      this.router.navigate(['welcome', this.username])
      this.invalidLogin = false
      },
      error => {
        console.log(this.username);
        this.invalidLogin = true;
      }
    )
      
    }

    handleJWTAuthLogin() {

      // if(this.username === "kirito" && this.password === 'dummy'){
      this.basicAuthService.executeJWTAuthenticateService(this.username, this.password)
      .subscribe(
        success =>{
          console.log(success)
                
        //Redirect to welcome page
        this.router.navigate(['welcome', this.username])
        this.invalidLogin = false
        },
        error => {
          console.log(this.username);
          this.invalidLogin = true;
        }
      )
        
      }



  }
