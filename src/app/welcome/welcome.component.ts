import { Component, OnInit } from '@angular/core';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';
import { ActivatedRoute } from '@angular/router';
import { WelcomeDataService } from '../service/data/welcome-data.service';
//import {AppComponent} from '../app.component';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  message : string = 'Welcome kirito asuna'
  name : String =  '';
  welcomeMessage: string
  //ActivatedRoute
  constructor(private route:ActivatedRoute,
    private service:WelcomeDataService
    ) { }

  ngOnInit() : void{
    
    console.log(this.message);
    //console.log(this.route.snapshot.params['name']);
    this.name = this.route.snapshot.params['name'];
  }

  getWelcomeMessage() {
    this.service.executeHelloWorldBeanService();
    //console.log("Get welcome message");
    this.service.executeHelloWorldBeanService().subscribe(
      response => this.handleSuccessfulResponse(response),
      error => this.handleErrorResponse(error)
      );
    //console.log('last line of getWelcomeMessage')
  }


  getWelcomeMessageWithParameter() {
    //this.service.executeHelloWorldServiceWithPathVariable(this.name);
    //console.log("Get welcome message");
    this.service.executeHelloWorldServiceWithPathVariable(this.name).subscribe(
      response => this.handleSuccessfulResponse(response),
      error => this.handleErrorResponse(error)
      );
    //console.log('last line of getWelcomeMessage')
  }


  handleSuccessfulResponse(response){
    this.welcomeMessage = response.message
    //console.log(response);
    //console.log(response.message);
  }

  handleErrorResponse(error){
    this.welcomeMessage = error.message
    //console.log(error.message);
  }

}
